//@ format points to svg
function pointsToString(points) {
    let pointsText = "M" + points[0].x + " " + points[0].y + " ";
    for (var i = 1; i < points.length; i++) {
        pointsText = pointsText + "L" + points[i].x + " " + points[i].y + " ";
    }
    pointsText = pointsText + "L" + points[0].x + " " + points[0].y + " Z";;
    return pointsText;
}
//@ calculate number of points in figure
function getArea(figure, minX, maxX, minY, maxY) {
    let nameDIV= window.nameDIV;
    let nameInput = "input_svg_" + nameDIV;
    let nameSVG = "svg_element_" + nameDIV;
    let svge = document.getElementById(nameSVG);

    let areaTotal = 0;
    let areaPercent = {};
    for (let p = 0; p < 101; p++) {
        areaPercent[p] = 0;
    }
    let xStep = 0;
    let areaX = 0;
    let newPoint = null;
    for (let i = minX; i <= maxX; i++) {
        let xPercent = Math.round(i * 100 / maxX);
        if (xPercent > xStep) {
            xStep = xPercent;
            areaX = 0;
        }
        for (let j = minY; j <= maxY; j++) {
	    newPoint = svge.createSVGPoint();
	    newPoint.x=i;
	    newPoint.y=j;

            if (figure.isPointInFill(newPoint) || figure.isPointInStroke(newPoint)) {
                areaTotal++;
                areaX++;
                areaPercent[xPercent] = areaX;


            }
        }
    }
    areaPercent[0] = areaPercent[0] * 100 / areaTotal;
    for (let p = 1; p < 101; p++) {
        areaPercent[p] = areaPercent[p - 1] + areaPercent[p] * 100 / areaTotal;
    }

    return {
        "areaTotal": areaTotal,
        "areaPercent": areaPercent
    };
}

function selectFunction(){
    if (window.activeSelection){
	window.activeSelection= false;
    }else{
	window.activeSelection=true;
    }
}
function downFunction(evt) {
    let nameDIV=window.nameDIV;
    let nameInput = "input_svg_" + nameDIV;
    let nameSVG = "svg_element_" + nameDIV;
    let nameDrawSVG = "polygonSVG_" + nameDIV;
    let selectorNameDrawSVG = "#" + nameDrawSVG;
    let nameGradient = "gradientSVG_" + nameDIV;
    let selectorNameGradient = "#" + nameGradient;
    let nameStopColor = "stopColorSVG_" + nameDIV;
    let nameStopBack = "stopBackSVG_" + nameDIV;
    let ps = document.getElementById(nameDrawSVG);

    let mg = document.getElementById(nameGradient);
    let sc = document.getElementById(nameStopColor);
    let sb = document.getElementById(nameStopBack);
    let isvg = document.getElementById(nameInput);
    if (window.activeSelection){
	let e = evt.target;
	let dim = e.getBoundingClientRect();
	//let mouseX = evt.clientX;
	let mouseX = evt.clientX - dim.left;
	let mouseY = evt.clientY - dim.top;
	/*    var offset = document.querySelector(selectorNameDrawSVG).getBoundingClientRect();
	      if (!mie) {
              mouseX = e.pageX - offset.left;
              mouseY = e.pageY - offset.top;
	      } else {
              mouseX = e.pageX - offset.left;
              mouseY = e.pageY - offset.top;
	      }*/
	let posX = Math.round((mouseX ) * 100 / (window.max_ps_x ));
	let posX_not_round = (mouseX ) * 100 / (window.max_ps_x );
	let percent_X = 0;
	if (posX <= 0) {
            posX = 0;
            percent_X = 0;
	} else if (posX > 100) {
            posX = 100;
            percent_X = Math.round(window.areaFigure.areaPercent[posX]);
	} else {
            percent_X = Math.round(window.areaFigure.areaPercent[posX]);
	}

	let percent_X_text = percent_X + "%";
	let pos_X_text = posX_not_round + "%";
	let percent_X_text_opp = (100 - percent_X) + "%";

	console.log ("AREA PUNTOS",window.areaFigure,evt.clientX,dim.left,percent_X,posX,posX_not_round,mouseX);
	sc.setAttribute("offset", pos_X_text);
	//answer.setAttribute("value", percent_X);
	isvg.setAttribute("value", noLineal(percent_X));
	//Qualtrics.SurveyEngine.setEmbeddedData("peaks_figure_value", percent_X);
	//thisQuestion.enableNextButton();
	//sb.setAttribute("offset",percent_X_text_opp);
	//sc.style.backgroundImage = "url('#myGradient')";
    }
}
function flip(pointsF){
    // get min and max X coordinate
    let xMin=1000000000;
    let xMax=0;
    for (var i=0;i<pointsF.length;i++){
	if (pointsF[i].x<xMin){
	    xMin=pointsF[i].x;
	}
	if (pointsF[i].x>xMax){
	    xMax=pointsF[i].x;
	}
    }
    //flip points horizontally
    let pointMiddle=(xMin+xMax)/2
    let flipPoints=new Array();
    let flipPoint=null;
    for (var i=0;i<pointsF.length;i++){
	flipPoint={x:Math.round(2*pointMiddle-pointsF[i].x),y:pointsF[i].y};
	flipPoints.push(flipPoint);
    }
    //sort flipPoints
    let newPoints=new Array();
    for (var i=flipPoints.length-1;i<0;i=i-1){
	newPoints.push(flipPoints[i]);
    }
    console.log("Flipando",pointsF,flipPoints,newPoints);
    return flipPoints;
}
// parameters
function figurePeak(){
    let points = [
	{
            x: 0,
            y: 80
	},
	{
	    x: 35,
	    y: 30
	},
	{
	    x: 45,
	    y: 10
	},
	{
	    x: 55,
	    y: 20
	},
	{
	    x: 80,
	    y: 5
	},
	{
	    x: 150,
	    y: 60
	},
	{
	    x: 320,
	    y: 80
	},
	{
	    x: 150,
	    y: 90
	},
	{
	    x: 140,
	    y: 110
	},
	{
	    x: 130,
	    y: 95
	},
	{
	    x: 110,
	    y: 100
	},
	{
	    x: 85,
	    y: 115
	},
	{
	    x: 95,
	    y: 130
	},
	{
	    x: 75,
	    y: 125
	},
	{
	    x: 70,
	    y: 180
	},
	{
	    x: 45,
	    y: 125
	},
	{
	    x: 35,
	    y: 160
	},
	{
	    x: 20,
	    y: 115
	}
    ];
    return points;
}

function figurePeakFlip(){
    let pointsToFlip = [
	{
	    x: 0,
	    y: 80
	},
	{
            x: 35,
            y: 30
	},
	{
            x: 45,
            y: 10
	},
	{
            x: 55,
            y: 20
	},
	{
            x: 80,
            y: 5
	},
	{
            x: 150,
            y: 60
	},
	{
            x: 320,
            y: 80
	},
	{
            x: 150,
            y: 90
	},
	{
            x: 140,
            y: 110
	},
	{
            x: 130,
            y: 95
	},
	{
            x: 110,
            y: 100
	},
	{
            x: 85,
            y: 115
	},
	{
            x: 95,
            y: 130
	},
	{
            x: 75,
            y: 125
	},
	{
            x: 70,
            y: 180
	},
	{
            x: 45,
            y: 125
	},
	{
            x: 35,
            y: 160
	},
	{
            x: 20,
            y: 115
	}
    ];
    let points=flip(pointsToFlip);

    return points;
}

function figurePeakFlipSmooth(){
    let pointsToFlip = [
	{
            x: 0,
            y: 50
	},
	{
            x: 30,
            y: 0
	},
	{
            x: 60,
            y: 50
	},
	{
            x: 140,
            y: 0
	},
	{
            x: 180,
            y: 50
	},
	{
            x: 220,
            y: 60
	},
	{
            x: 180,
            y: 200
	},
	{
            x: 140,
            y: 240
	},
	{
            x: 60,
            y: 70
	},
	{
            x: 30,
            y: 90
	},
	{
            x: 0,
            y: 50
	}

    ];
    let points=flip(pointsToFlip);

    return points;
}

function figurePeakSmooth(){
    let points = [
	{
            x: 0,
            y: 50
	},
	{
            x: 30,
            y: 0
	},
	{
            x: 60,
            y: 50
	},
	{
            x: 140,
            y: 0
	},
	{
            x: 180,
            y: 50
	},
	{
            x: 220,
            y: 60
	},
	{
            x: 180,
            y: 200
	},
	{
            x: 140,
            y: 240
	},
	{
            x: 60,
            y: 70
	},
	{
            x: 30,
            y: 90
	},
	{
            x: 0,
            y: 50
	}

    ];

    return points;
}


function figureCircle(x,y,r){
    // get points to circle center in x,y radius r

    let points=new Array();
    let xInf=x-r;
    let xSup=x+r;
    let j=0;
    for (let i=xInf;i<xSup;i=i+1){
	j=y+Math.sqrt(Math.pow(r,2)-Math.pow(i-x,2));
	points.push({x:i,y:j});
    }
    for (let i=xSup;i>=xInf;i=i-1){
	j=y-Math.sqrt(Math.pow(r,2)-Math.pow(i-x,2));
	points.push({x:i,y:j});
    }
    return points;
}

function addHTML(textPoints){
    /* put code html for svg */
    let nameDIV=window.nameDIV;
    let div = document.getElementById(nameDIV);
    let nameInput = "input_svg_" + nameDIV;
    let nameSVG = "svg_element_" + nameDIV;
    let nameDrawSVG = "polygonSVG_" + nameDIV;
    let selectorNameDrawSVG = "#" + nameDrawSVG;
    let nameGradient = "gradientSVG_" + nameDIV;
    let selectorNameGradient = "#" + nameGradient;
    let nameStopColor = "stopColorSVG_" + nameDIV;
    let nameStopBack = "stopBackSVG_" + nameDIV;
    let colorFill = "#0000FF";
    let colorBack = "#FFFFFF";

    div.innerHTML = '<input disabled id="' + nameInput + '" type="text"/><svg id="' + nameSVG + '"  height="310" width="500"><defs><linearGradient id="' + nameGradient + '"  x1="0%" y1="0%" x2="100%" y2="0%">      <stop id ="' + nameStopColor + '" offset="0%"  stop-color="' + colorFill + '" />      <stop id ="' + nameStopBack + '" offset="0%" stop-color="' + colorBack + '" />    </linearGradient>  </defs>  <path  id="' + nameDrawSVG + '" d="' + textPoints + '" style="fill:url(\'' + selectorNameGradient + '\');stroke:purple;stroke-width:1;" fill="' + colorBack + '"/>  Sorry, your browser does not support inline SVG.</svg>';

}

function AddEvent(points){
    /* add events over figure*/
    let maxX = 0;
    let minX = 0;
    let maxY = 0;
    let minY = 0;
    //calculate Maximus
    for (let i = 0; i < points.length; i++) {
	if (points[i].x > maxX) {
            maxX = points[i].x;
	}
	if (points[i].y > maxY) {
            maxY = points[i].y;
	}
    }
    console.log("Calculate Max", maxX,maxY);


    window.activeSelection=false;
    /* qualtrics
       this.disableNextButton();
       let ds = this.getChoiceContainer();
       ds.children[0].style.visibility = "hidden";
       let id = ds.children[0].id;*/
    let nameDIV= window.nameDIV;
    let nameInput = "input_svg_" + nameDIV;
    let nameSVG = "svg_element_" + nameDIV;
    let nameDrawSVG = "polygonSVG_" + nameDIV;
    let selectorNameDrawSVG = "#" + nameDrawSVG;
    let nameGradient = "gradientSVG_" + nameDIV;
    let selectorNameGradient = "#" + nameGradient;
    let nameStopColor = "stopColorSVG_" + nameDIV;
    let nameStopBack = "stopBackSVG_" + nameDIV;

    let isvg = document.getElementById(nameInput);
    let mie = (navigator.appName == "Microsoft Internet Explorer") ? true : false;


    let ps = document.getElementById(nameDrawSVG);

    let mg = document.getElementById(nameGradient);
    let sc = document.getElementById(nameStopColor);
    let sb = document.getElementById(nameStopBack);
    let mouseX = 0;
    let mouseY = 0;
    let svge = document.getElementById(nameSVG);

    window.max_ps_x = maxX;
    let thisQuestion = this;
    let min_ps_x = minX;

    window.areaFigure = getArea(ps, minX, maxX, minY, maxY);




    if (window.PointerEvent) {
	svge.addEventListener("pointermove", downFunction);
    } else {
	svge.addEventListener("mousemove", downFunction);

    }
    svge.addEventListener("click", selectFunction);
}
function noLineal(x){
    /* return a value from a nolineal function */
    let norm=100/Math.pow(100,2);
    let f=Math.round(Math.pow(x,2)*norm);
    return f;
}
function initFigure(){
    const urlParams = new URLSearchParams(window.location.search);
    let points=null;
    let figureFunc=figurePeak;//figure default
    if (urlParams.has('figure')){
	figureFunc = window[urlParams.get('figure')];
    }
    points=figureFunc();//function with the point


    let textPoints = pointsToString(points);
    let nameDIV="app";
    window.nameDIV=nameDIV;
    addHTML(textPoints);
    AddEvent(points);
}
window.onload=initFigure;
